#include <iostream>
#include <string>
#include <variant>

#include "catch.hpp"

using namespace std;

TEST_CASE("variant")
{        
    SECTION("basic usage")
    {
        variant<int, string, double> var;

        REQUIRE(get<int>(var) == 0);
        REQUIRE(var.index() == 0);
        REQUIRE(holds_alternative<int>(var));

        var = 3.14;

        REQUIRE(get<double>(var) == 3.14);
        REQUIRE(var.index() == 2);
        REQUIRE_FALSE(holds_alternative<int>(var));
        REQUIRE(holds_alternative<double>(var));

        var = "text"s;

        auto ptr_int = get_if<int>(&var);
        REQUIRE(ptr_int == nullptr);

        auto ptr_str = get_if<string>(&var);
        REQUIRE(*ptr_str == "text"s);

        REQUIRE(var.index() == 1);
        REQUIRE(get<string>(var) == "text"s);

        REQUIRE_THROWS_AS(get<int>(var), bad_variant_access);
    }
}

struct X
{
    int value;

    explicit X(int v) : value{v}
    {}
};

struct Y
{
    int value;

    explicit Y(int v) : value{v}
    {}
};


TEST_CASE("monostate when no default constructor for types on a list")
{
    variant<monostate, X, Y> var;

    REQUIRE(holds_alternative<monostate>(var));
}

TEST_CASE("implicit conversion can cause troubles")
{
    variant<string, double, bool> var = "text";

    REQUIRE(var.index() == 2);

    cout << get<bool>(var) << endl;
}

TEST_CASE("can hold the same type more than once")
{
    variant<int, string, int> var = "text"s;

    var.emplace<0>(42);

    REQUIRE(var.index() == 0);

    var.emplace<2>(665);

    REQUIRE(var.index() == 2);
}


////////////////////////////////////////////////
// Visiting variants

struct Printer
{
    template <typename T>
    void operator()(T x) const
    {
        cout << typeid(T).name() << ": " <<  x << endl;
    }

    void operator()(const vector<int>& vec) const
    {
        cout << "vec: ";
        for(const auto& item : vec)
            cout << item << " ";
        cout << endl;
    }
};


TEST_CASE("visiting variants")
{
    variant<int, string, double, vector<int>> var = "42"s;

    visit(Printer{}, var);
}

// local visitors

template <typename... Closure>
struct Overloader : Closure...
{
    using Closure::operator()...;
};

template <typename... Closure>
Overloader(Closure...) -> Overloader<Closure...>;

TEST_CASE("oveloader + variant")
{
    variant<int, double, vector<int>> var = vector{1, 2, 3 };

    const string prefix = "PREFIX";

    auto local_printer = Overloader {
        [=](auto x) { cout << prefix << "item: " << x << endl; },
        [=](const vector<int>& vec) { cout << prefix << "vec: " << vec.size() << endl; }
    };

    visit(local_printer, var);
}

struct ErrorCode
{
    string description;
};

[[nodiscard]] variant<string, ErrorCode> load_file(const string& filename)
{
    if (filename.length() > 5)
        return ErrorCode{"file to long"};

    return "File content: "s + filename;
}

TEST_CASE("use case for variant")
{
    visit(
        Overloader{
            [](const string& content) {
                cout << content << endl;
            }
            ,
            [](ErrorCode ec) {
                cout << ec.description << endl;
            }
        },
        load_file("abcfsdfsdfsdf"));
}
