#include <iostream>
#include <string>
#include <thread>
#include <future>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

enum class ResultCode
{
    ok,
    bad_bit,
    file_corrupt,
    unknown,
    error [[deprecated]] = unknown
};

[[nodiscard]] ResultCode save_to_file()
{
    //...

    return ResultCode::bad_bit;
}

TEST_CASE("nodiscard")
{
    save_to_file();

    ResultCode result = save_to_file();

    switch(result)
    {
        case ResultCode::bad_bit:
            cout << "bad_bit found" << endl;
            [[fallthrough]]
        case ResultCode::file_corrupt:
            cout << "file is broken" << endl;
            break;
        case ResultCode::error:
            cout << "Error found" << endl;
            break;
        defualt:
            cout << "OK" << endl;
    }
}


void save_file(const string& file_name)
{
    cout << "Starting save " << file_name << endl;

    this_thread::sleep_for(1000ms);

    cout << "End of save " << file_name << endl;
}

TEST_CASE("async bug")
{
    auto f1 = async(launch::async, save_file, "a.txt");
    auto f2 = async(launch::async, save_file, "b.txt");
    auto f3 = async(launch::async, save_file, "c.txt");
}


TEST_CASE("maybe_unused")
{
    [[maybe_unused]] int x = 10;
}
