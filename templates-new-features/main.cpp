#include <iostream>
#include <string>
#include <tuple>
#include <map>

#include "catch.hpp"

using namespace std;

class Customer
{
    int id_;
    string name_;

public:
    Customer(int id, string name) : id_{id}, name_{std::move(name)}
    {}

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    auto tied() const
    {
        return tie(id_, name_);
    }
};

struct CustomerEq
{
    bool operator()(const Customer& c1, const Customer& c2) const
    {
        return c1.tied() == c2.tied();
    }
};

template <typename T>
void hash_combine(size_t& seed, const T& value)
{
    seed ^= hash<T>{}(value) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

template <typename... Ts>
size_t combined_hash(const Ts&... value)
{
    size_t seed = 0;
    (..., hash_combine(seed, value));

    return seed;
}

struct CustomerHash
{
    size_t operator()(const Customer& c) const
    {
        return combined_hash(c.id(), c.name());
    }
};


template <typename... Ts>
struct Overloader : Ts...
{
    using Ts::operator()...; // Since C++17
};

TEST_CASE("")
{
    Customer c{1, "Jan"s};

    using CustomerComparer = Overloader<CustomerEq, CustomerHash>;

    CustomerComparer cc;

    auto hashed_value = cc(c);

    cout << hashed_value << endl;

    cout << cc(c, Customer{2, "Adam"s}) << endl;
}


decltype(auto) find_by_key(map<int, string>& dict, int key)
{
    return dict.at(key);
}

TEST_CASE("auto vs decltype(auto)")
{
    map<int, string> dict = { {1, "one"} };

    find_by_key(dict, 1) = "jeden"s;

    REQUIRE(dict[1] == "jeden"s);
}

