#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
    tuple<int, int, double> calc_stats(const vector<int>& data)
    {
        vector<int>::const_iterator min, max;
        tie(min, max) = minmax_element(begin(data), end(data));

        double avg = accumulate(begin(data), end(data), 0.0) / data.size();

        return make_tuple(*min, *max, avg);
    }
}

tuple<int, int, double> calc_stats(const vector<int>& data)
{
    auto[min, max] = minmax_element(begin(data), end(data));

    double avg = accumulate(begin(data), end(data), 0.0) / data.size();

    return make_tuple(*min, *max, avg);
}

struct Date
{
    int y;
    int d;
    std::string m;

    std::string to_string() const
    {
        return "Date("s + std::to_string(y) + "," + std::to_string(d) + "," + m + ")";
    }

    inline static int id = 1;
};

TEST_CASE("structured bindings")
{
    vector<int> data = {1, 76, 234, 123, 0, 41341};

    SECTION("before c++17")
    {
        int min, max;
        double avg;

        tie(min, max, std::ignore) = BeforeCpp17::calc_stats(data);

        cout << "min: " << min << endl;
        cout << "max: " << max << endl;
        // cout << "avg: " << avg << endl;
    }

    SECTION("allows unpack a tuple into variables")
    {
        auto[min, max, avg] = BeforeCpp17::calc_stats(data);

        cout << "min: " << min << endl;
        cout << "max: " << max << endl;
        cout << "avg: " << avg << endl;
    }

    SECTION("allows unpack members of struct")
    {
        Date today{2018, 1, "Aug"};

        auto[year, day, month] = today;

        REQUIRE(year == 2018);
        REQUIRE(day == 1);
        REQUIRE(month == "Aug"s);
    }

    SECTION("allow unpack items of native array")
    {
        auto get_point = []() -> int(&)[3]
        {
            static int coordinates[3] = {1, 2, 3};
            return coordinates;
        };

        auto[x, y, z] = get_point();

        REQUIRE(x == 1);
        REQUIRE(y == 2);
        REQUIRE(z == 3);

        string data[2] = {"one", "two"};

        auto[first, second] = data;

        REQUIRE(first == "one"s);
    }

    SECTION("&, const, volatile modifiers are allowed")
    {
        tuple<int, string, double> tpl{1, "text"s, 3.14};

        SECTION("reference")
        {
            auto & [ first, second, third ] = tpl;

            first = 2;

            REQUIRE(get<0>(tpl) == 2);

            auto && [ min, max, avg ] = calc_stats(data);
        }

        SECTION("alignas")
        {
            alignas(16) auto[first, second, third] = tpl;
        }

        SECTION("* is not allowed")
        {
            //auto* [f, s, t] = tpl;  // ERROR - ill-formed
        }
    }
}

struct X
{
    int (*ptr_fun)(int);
    int n;
};

int my_negate(int x)
{
    return -x;
}

TEST_CASE("test for X")
{
    X x{&my_negate, 3};

    auto[fun, value] = x;

    REQUIRE(fun(value) == -3);
}

TEST_CASE("Use cases")
{
    map<int, string> dictionary = {{1, "one"}, {2, "two"}, {3, "three"}};

    SECTION("iteration over map")
    {
        for (const auto & [ key, value ] : dictionary)
        {
            cout << key << " - " << value << endl;
        }
    }

    SECTION("inserting into map")
    {
        if (auto[pos, is_success] = dictionary.insert(make_pair(6, "six")); is_success)
        {
            const auto & [ key, value ] = *pos;

            cout << "Item " << key << " was inserted" << endl;
        }
    }
}

////////////////////////////////////////////////////
// tuple protocol for structured bindings

enum Something
{
};

// 1-st step

template <>
struct std::tuple_size<Something>
{
    static const unsigned int value = 2;
};

// 2-nd step

template <>
struct std::tuple_element<0, Something>
{
    using type = int;
};

template <>
struct std::tuple_element<1, Something>
{
    using type = double;
};

// 3-rd step

namespace TemplateSpecialization
{
    template <size_t>
    auto get(const Something&);

    template <>
    auto get<0>(const Something&)
    {
        return 42;
    }

    template <>
    auto get<1>(const Something&)
    {
        return 3.14;
    }
}

template <size_t N>
auto get(const Something&)
{
    if constexpr(N == 0)
        return 42;
    else
        return 3.14;
}

TEST_CASE("structured binding with tuple protocol")
{
    auto[first, second] = Something{};

    REQUIRE(first == 42);
    REQUIRE(second == Approx(3.14));
}
