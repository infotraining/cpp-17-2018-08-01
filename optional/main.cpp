#include <iostream>
#include <optional>
#include <string>

#include "catch.hpp"

using namespace std;

TEST_CASE("optional")
{
    optional<int> opt_int;

    REQUIRE_FALSE(opt_int.has_value());

    optional<string> text1 = "text";
    REQUIRE(text1.has_value());

    optional text2 = "text"s;
    REQUIRE(text2.has_value());

    SECTION("getting to value")
    {
        SECTION("unsafe")
        {
            auto& value = *text1;
            REQUIRE(value == "text"s);
        }

        SECTION("safe")
        {
            auto holded_text = text2.value();

            SECTION("when optional is empty value() throws exception")
            {
                optional<string> opt_str;

                REQUIRE_THROWS_AS(opt_str.value(), std::bad_optional_access);
            }
        }
    }

    SECTION("value_or_default")
    {
        optional<string> opt_str;

        auto result = opt_str.value_or("Empty"s);

        REQUIRE(result == "Empty"s);

        opt_str = ""s;

        REQUIRE(opt_str.value_or("Empty"s) == ""s);

        opt_str = std::nullopt; // reset
    }
}

namespace Since_Gcc_8_2
{
//    optional<int> as_int(string_view sv)
//    {
//        int result{};

//        if (auto[str_buffer, err_code] = std::from_chars(begin(sv), end(sv), result); err_code == std::errc{0})
//        {
//            return result;
//        }

//        return std::nullopt;
//    }
}


