#include <iostream>
#include <string>

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
    template <typename Arg>
    Arg sum(Arg arg)
    {
        return arg;
    }

    template <typename Arg, typename... Args>
    Arg sum(Arg arg, Args... args)
    {
        return arg + sum(args...);
    }
}

template <typename... Args>
auto sum(Args... args)
{
    return (... + args); // (((1 + 2) + 3) + 4) // LEFT FOLD
}

template <typename... Args>
auto sum_r(Args... args)
{
    return (args + ...); // (1 + (2 + ( 3 + 4))) // RIGHT FOLD
}


TEST_CASE("Before C++17")
{    
    cout << "sum: " << BeforeCpp17::sum(1, 4, 5, 6, 7, 23, 234, 233) << endl;
}

TEST_CASE("Cpp17")
{
    cout << "sum: " << sum(1, 4, 5, 6, 7, 23, 234, 233) << endl;
    cout << "sum: " << sum_r(1, 4, 5, 6, 7, 23, 234, 233) << endl;

    cout << "text:" << sum("text"s, "hello", "world") << endl;
}

template <typename... Args>
bool all_true(Args... args)
{
    return (... && args);
}

TEST_CASE("all_true")
{
    REQUIRE(all_true(1, 1, 1, true, true));
    REQUIRE_FALSE(all_true(1, false, 1, true));
}

namespace BeforeCpp17
{
    void print()
    {
        cout << endl;
    }

    template <typename Head, typename... Tail>
    void print(Head head, Tail... tail)
    {
        cout << head << " ";

        print(tail...);
    }
}

template  <typename... Args>
void print(Args... args)
{
    auto insert_space_before = [](const auto& item) {
        cout << " ";
        return item;
    };

    (cout << ... << insert_space_before(args)) << "\n";
}

TEST_CASE("print")
{
    print(1, 3.14, "text");
}

////////////////////////////////////////////////////////
///

struct Window
{
    void show() { std::cout << "showing Window\n"; }
};

struct Widget
{
    void show() { std::cout << "showing Widget\n"; }
};

struct Toolbar
{
    void show() { std::cout << "showing Toolbar\n"; }
};

TEST_CASE("call funtions for all objects")
{
    Window wnd;
    Widget widget;
    Toolbar toolbar;

    auto printer = [](auto&&... args) { (forward<decltype(args)>(args).show(), ...); };

    printer(wnd, widget, toolbar);
}

/////////////////////////////////////////////////////////////////
// Fold expression in deduction guide

template <typename T, size_t N>
struct Array
{
    T items[N];

    using iterator = T*;
    using const_iterator = const T*;
    using value_type = T;
    //...

    constexpr const_iterator cbegin() const
    {
        return items;
    }

    constexpr const_iterator cend() const
    {
        return items + N;
    }

    constexpr size_t size() const
    {
        return N;
    }

    //...
};

template <typename Head, typename... Tail>
Array(Head, Tail...) -> Array<enable_if_t<(is_same_v<Head, Tail> && ...), Head>, 1 + sizeof...(Tail)>;


TEST_CASE("deduction guide for an Array")
{
    Array arr1 = { 1, 2, 3, 4, 6, 5 };

    static_assert (arr1.size() == 6);
}
