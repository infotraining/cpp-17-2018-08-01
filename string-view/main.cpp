#include <iostream>
#include <string>
#include <string_view>

#include "catch.hpp"

using namespace std;

TEST_CASE("string_view")
{
    const char* ctext = "abc"; // null-terminated
    string text = "abc"s; // null-terminated
    string_view text_sv = "text"sv;

    SECTION("conversions")
    {

        SECTION("from const char*")
        {
            string_view sv1 = "text"; //
        }

        SECTION("from string")
        {
            string text = "abc";
            string_view sv2 = text; // implicit converion with string::operator string_view

            SECTION("BEWARE")
            {
                string_view sv{"text"s};

                cout << sv << endl;
            }
        }
    }
}

void api_fun(string_view filename)
{
    cout << "called api_fun: " << filename << endl;
}

string get_file_name()
{
    return "filename"s;
}

TEST_CASE("using string_view as params")
{
    api_fun("text");

    string file_name{"TEXT"};

    api_fun(file_name);

    SECTION("BEWARE")
    {
        api_fun(get_file_name()); // UB
    }
}
