# README #

### Docs ###

* https://infotraining.bitbucket.io/cpp-17/

### Test before training ###

* https://goo.gl/forms/tdUPr4ASH9CAjob43

### Proxy settings ###

* add to .profile

```
export http_proxy=http://10.158.100.2:8080
export https_proxy=https://10.158.100.2:8080
```

### Test after training ###

* https://goo.gl/forms/FWmJvnO6HUiFdUBK2

### Ankieta ####

* https://infotraining.pl/ankieta/cpp-17-2018-08-01-kp