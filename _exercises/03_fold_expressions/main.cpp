#include <iostream>
#include <string>
#include <vector>
#include <set>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"


using namespace std;

template <typename Container, typename... T>
size_t matches(const Container& c, T... value)
{
    return (... + count(begin(c), end(c), value));
}

TEST_CASE("matches - returns how many items is stored in a container")
{
    vector<int> v{1, 2, 3, 4, 5};

    REQUIRE(matches(v, 2, 5) == 2);
    REQUIRE(matches(v, 100, 200) == 0);
    REQUIRE(matches("abcdef", 'x', 'y', 'z') == 0);
    REQUIRE(matches("abcdef", 'a', 'd', 'f') == 3);
}

template <typename Container, typename... T>
void push_all(Container& container, T&&... value)
{
    (container.push_back(forward<T>(value)), ...);
}

TEST_CASE("push_all - multiple push_backs")
{
    using namespace Catch::Matchers;

    std::vector<int> v {1, 2, 3};
    push_all(v, 4, 5, 6);

    REQUIRE_THAT(v, Equals(vector {1, 2, 3, 4, 5, 6}));
}

template <typename Set, typename... T>
bool insert_all(Set& container, T&&... value)
{
    return (... && container.insert(std::forward<T>(value)).second);
}

TEST_CASE("insert_all - performs inserts and returns true if all have been succesful")
{
    set<int> my_set = { 1, 2, 3 };

    REQUIRE(insert_all(my_set, 4, 5, 6) == true);
    REQUIRE(insert_all(my_set, 6, 3) == false);
}

template <typename T>
struct range : pair<T, T>
{
    range(const T& left, const T& right) : pair<T, T>{left, right}
    {
    }
};

template <typename T1, typename T2>
range(T1, T2) -> range<common_type_t<T1, T2>>;

template <typename T, typename... Ts>
bool within(const range<T>& rng, const Ts&... value)
{
    static_assert(sizeof...(value) >= 1);

    auto is_within = [&rng](const auto& v) { return v >= rng.first && v <= rng.second; };

    return (... && is_within(value));
}

TEST_CASE("within - checks if all values fit in range [low, high]")
{
    REQUIRE(within(range{10,  20},  1, 15, 30) == false);
    REQUIRE(within(range{10,  20},  11, 12, 13) == true);
    REQUIRE(within(range{5, 5.5},  5.1, 5.2, 5.3) == true);
    //within(range{5, 5.5});
}

template <typename T>
void hash_combine(size_t& seed, const T& value)
{
    seed ^= hash<T>{}(value) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

template <typename... Ts>
size_t combined_hash(const Ts&... value)
{
    size_t seed = 0;
    (..., hash_combine(seed, value));

    return seed;
}

TEST_CASE("combined_hash - write a function that calculates combined hash value for a given number of arguments")
{
    REQUIRE(combined_hash(1U) == 2654435770U);
    REQUIRE(combined_hash(1, 3.14, "string"s) == 10365827363824479057U);
    REQUIRE(combined_hash(123L, "abc"sv, 234, 3.14f) == 162170636579575197U);

    struct X
    {
        int a;
        double b;
        string text;
    };

    X x{1, 3.14, "text"};

    size_t seed{};

    hash_combine(seed, x.a);
    hash_combine(seed, x.b);
    hash_combine(seed, x.text);

    auto hashed_x = seed;
}
