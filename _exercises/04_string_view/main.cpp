#include <algorithm>
#include <iostream>
#include <set>
#include <string>
#include <string_view>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
    vector<string> split(const string& text, const string& delims = " ,")
    {
        vector<string> tokens;

        auto it1 = cbegin(text);

        while (it1 != cend(text))
        {
            auto it2 = find_first_of(it1, cend(text), cbegin(delims), cend(delims));

            tokens.emplace_back(it1, it2);

            if (it2 == cend(text))
                break;

            it1 = std::next(it2);
        }

        return tokens;
    }
}

vector<string_view> split_text(string_view text, string_view delims = " ,")
{
    vector<string_view> tokens;

    auto pos1 = cbegin(text);

    while(pos1 != cend(text))
    {
        auto pos2 = find_first_of(pos1, cend(text), cbegin(delims), cend(delims));

        tokens.emplace_back(pos1, pos2-pos1);

        if (pos2 == cend(text))
            break;

        pos1 = std::next(pos2);
    }

    return tokens;
}

TEST_CASE("split with spaces")
{
    string text = "one two three four";

    auto words = split_text(text);

    auto expected = {"one", "two", "three", "four"};

    REQUIRE(equal(begin(expected), end(expected), begin(words)));
}

TEST_CASE("split with commas")
{
    string text = "one,two,three,four";

    auto words = split_text(text);

    auto expected = {"one", "two", "three", "four"};

    REQUIRE(equal(begin(expected), end(expected), begin(words)));
}
