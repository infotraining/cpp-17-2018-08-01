#include <iostream>
#include <string>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

class Customer
{
private:
    std::string first_;
    std::string last_;
    u_int8_t age_;

public:
    Customer(std::string f, std::string l, u_int8_t v)
        : first_{std::move(f)}
        , last_{std::move(l)}
        , age_{v}
    {
    }

    std::string first() const
    {
        return first_;
    }

    std::string last() const
    {
        return last_;
    }

    u_int8_t age() const
    {
        return age_;
    }

    auto tied()
    {
        return tie(first_, last_, age_);
    }
};

template <>
struct std::tuple_size<Customer>
{
    static const size_t value = 3;
};

template <>
struct std::tuple_element<0, Customer>
{
    using type = std::string;
};

template <>
struct std::tuple_element<1, Customer>
{
    using type = std::string;
};

template <>
struct std::tuple_element<2, Customer>
{
    using type = u_int8_t;
};

template <size_t>
auto get(const Customer& c);

template <>
auto get<0>(const Customer& c)
{
    return c.first();
}

template <>
auto get<1>(const Customer& c)
{
    return c.last();
}

template <>
auto get<2>(const Customer& c)
{
    return c.age();
}

TEST_CASE("application of structured binding for Customer type")
{
    Customer c("Jan", "Kowalski", 42);

    auto[first_name, last_name, age] = c;

    REQUIRE(first_name == "Jan");
    REQUIRE(last_name == "Kowalski");
    REQUIRE(age == 42);
}

TEST_CASE("structured binding through tie")
{
    Customer c("Jan", "Kowalski", 42);

    const auto&[first_name, last_name, age] = c.tied();

    REQUIRE(first_name == "Jan");
    REQUIRE(last_name == "Kowalski");
    REQUIRE(age == 42);
}
