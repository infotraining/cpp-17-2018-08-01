#include <iostream>
#include <string>
#include <variant>
#include <vector>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

struct Circle
{
    int radius;
};

struct Rectangle
{
    int width, height;
};

struct Square
{
    int size;
};

template <typename... Closure>
struct Overloader : Closure...
{
    using Closure::operator()...;
};

template <typename... Closure>
Overloader(Closure...)->Overloader<Closure...>;

TEST_CASE("visit a shape variant and calculate area")
{
    using Shape = variant<Circle, Rectangle, Square>;

    vector<Shape> shapes = {Circle{1}, Square{10}, Rectangle{10, 1}};   

    double total_area{};

    auto area_calulator = Overloader {
        [&](const Square& s) { total_area += s.size * s.size; },
        [&](const Rectangle& r) { total_area +=  r.height * r.width; },
        [&](const Circle& c) { total_area +=  3.14 * c.radius * c.radius; }
    };

    for(const auto& s : shapes)
    {
        visit(area_calulator, s);
    }

    REQUIRE(total_area == Approx(113.14));
}
