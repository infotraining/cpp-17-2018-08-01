#include <iostream>
#include <string>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

vector<int> create_tab()
{
    return vector<int>(1'000'000); // RVO - copy elision is mandatory in C++17
}

vector<int> load_tab()
{
    vector<int> v(1'000'000);

    // ...

    return v; // named-RVO - copy elision is optional
}

TEST_CASE("rvo")
{
    vector<int> tab = create_tab(); // since C++17 no copy-constructor or move-constructor called
}

struct NoCopyableAndMoveable
{
    vector<int> vec;

    NoCopyableAndMoveable(const NoCopyableAndMoveable&) = delete;
    NoCopyableAndMoveable(NoCopyableAndMoveable&&) = delete;
};

NoCopyableAndMoveable create_strange_obj()
{
    return NoCopyableAndMoveable{ { 1, 2, 3 } };
}

TEST_CASE("NoCopyableTest")
{
    auto so = create_strange_obj();

    REQUIRE(so.vec.size() == 3);
}
