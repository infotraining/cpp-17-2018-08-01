#include <iostream>
#include <string>
#include <functional>

#include "catch.hpp"

using namespace std;

struct Gadget
{
    int counter = 0;

    Gadget()
    {
        cout << "Gadget(" << this << ")" << endl;
    }

    Gadget(const Gadget& source) : counter{source.counter}
    {
        cout << "Gadget(cc: " << this << " " << " from " << & source << ")" << endl;
    }

    ~Gadget()
    {
        cout << "Destroying gadget " << this << endl;
    }

    void play()
    {
        ++counter;
    }

    auto report()
    {
        return [*this] { cout << "Gadget(" << this << " - counter: " << counter << ")" << endl; };
    }

};

TEST_CASE("capturing this")
{
    function<void()> report;

    {
        Gadget g;
        g.play();
        g.play();

        report = g.report();
    }

    report();
}

TEST_CASE("constexpr lambda")
{
    SECTION("implicit constexpr")
    {
        auto square = [](const auto& x) { return x * x; };

        array<int, square(8)> arr1 = {};
        static_assert(arr1.size() == 64);

        int tab[square(10)];
        static_assert(size(tab) == 100);
    }

    SECTION("explicit constexpr")
    {
        constexpr array<int, 64> values = { 1, 2, 3, 4, 5, 6, 7 };

        auto sum = [](const auto& data) constexpr {
            long result = 0;
            for(const auto& item : data)
                result += item;

            return result;
        };

        static_assert(sum(values) ==28);
    }
}

namespace Future
{
    template <typename Iter, typename Pred>
    constexpr auto find_if(Iter start, Iter end, Pred pred_f) -> Iter
    {
        for(Iter it = start; it != end; ++it)
            if (pred_f(*it))
                return it;
        return end;
    }
}

TEST_CASE("constexpr find_if")
{
    constexpr array<int, 8> data = { 1, 256, 1024, 6128 };
    constexpr auto size_that_fit = *Future::find_if(begin(data), end(data), [](auto size) { return size > 1000; });

    array<int, size_that_fit> arr = {};

    static_assert(size(arr) == 1024);
}
