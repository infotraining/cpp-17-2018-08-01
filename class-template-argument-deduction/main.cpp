#include <iostream>
#include <string>
#include <memory>
#include <functional>

#include "catch.hpp"

using namespace std;

namespace Simple
{

template <typename T1, typename T2>
struct Data
{
    T1 a;
    T2 b;

    Data(T1 a, T2 b)
        : a{a}
        , b{b}
    {
    }
};

template <typename T1, typename T2>
Data<T1, T2> make_data(T1 a, T2 b)
{
    return Data<T1, T2>{a, b};
}

}


TEST_CASE("class template argument deduction")
{
    using namespace Simple;

    Data<int, double> d1{1, 3.14};

    auto d2 = make_data("text"s, 8.22); // Data<string, double>

    SECTION("Since C++17")
    {
        Data d3{1, 3.14}; // Data<int, double>
        Data d4{"text"s, 3.55f}; // Data<string, float>

        SECTION("stl pair example")
        {
            pair<int, float> p1{1, 3.14};
            pair p2{1, 3.14f};

            static_assert(is_same_v<decltype(p1), decltype(p2)>);
        }
    }

    SECTION("array decay")
    {
        int tab[4] = {1, 2, 3, 4};
        Data d{"text", tab}; // Data<const char*, int*>

        static_assert(is_same_v<decltype(d), Data<const char*, int*>>);
    }

    SECTION("const")
    {
        const int cx = 665;

        Data d{cx, 13};

        static_assert(is_same_v<decltype(d), Data<int, int>>);
    }
}

void foo(int* tab, size_t)
{
}

TEST_CASE("tab decays to pointer when passed as argument")
{
    int tab[10];
    foo(tab, size(tab));
}

template <typename Param>
void deduce_type(Param& p)
{
}

TEST_CASE("auto deduction rules")
{
    SECTION("Case 1 - auto without ref")
    {
        int tab[10];
        auto arr = tab; // int*

        const int cx = 665;
        auto y       = cx; // int

        auto f = foo;
    }

    SECTION("Case 2 - auto with ref")
    {
        int tab[10];
        auto& arr = tab; // int(&)[10]

        const int cx = 665;
        auto& y      = cx; // const int&

        auto f = foo; // void(&)(int*, size_t)
    }
}

namespace WithoutDeductionGuides
{

    template <typename T1, typename T2>
    struct Pair
    {
        T1 first;
        T2 second;

        Pair(const T1& f, const T2& s) // : first{f}, second{s}
        {
        }
    };
}

TEST_CASE("deduction for constructor with ref")
{
    using namespace WithoutDeductionGuides;

    Pair p1{6, 3.14};
    static_assert(is_same_v<Pair<int, double>, decltype(p1)>);

    SECTION("array decay")
    {
        int tab[4] = {1, 2, 3, 4};
        Pair p{"text", tab}; // Data<const char*, int*>

        static_assert(is_same_v<decltype(p), Pair<char[5], int[4]>>);
    }

    SECTION("const")
    {
        const int cx = 665;

        Pair d{cx, 13};

        static_assert(is_same_v<decltype(d), Pair<int, int>>);
    }
}

namespace WithDeductionGuides
{

    template <typename T1, typename T2>
    struct Pair
    {
        T1 first;
        T2 second;

        Pair(const T1& f, const T2& s)
             : first{f}, second{s}
        {
        }
    };

    // deduction guide
    template<typename T1, typename T2> Pair(T1, T2) -> Pair<T1, T2>;
}

TEST_CASE("Pair with deduction guide")
{
    using namespace WithDeductionGuides;

    int tab[4] = {1, 2, 3, 4};
    Pair p{"text", tab};

    static_assert(is_same_v<decltype(p), Pair<const char*, int*>>);
}


namespace MoreComplexCase
{
    template <typename T>
    struct Data
    {
        T data;

        explicit Data(const T& item) : data{item}
        {}

        template <typename ItemT>
        Data(initializer_list<ItemT> items) : data{items}
        {
        }
    };

    template <typename T> Data(T) -> Data<T>;

    template <typename ItemT> Data(initializer_list<ItemT>) -> Data<vector<ItemT>>;

    Data(const char*) -> Data<string>;
}

TEST_CASE("more complex deduction guides")
{
    using namespace MoreComplexCase;

    Data d1(1);
    static_assert(is_same_v<decltype(d1), Data<int>>);

    Data d2 = { 1, 2, 3 };
    static_assert(is_same_v<decltype(d2), Data<vector<int>>>);

    Data d3{"text"};
    static_assert(is_same_v<decltype(d3), Data<vector<const char*>>>);

    Data d4("text");
    static_assert(is_same_v<decltype(d4), Data<string>>);
}


template <typename T>
struct Complex
{
    T real, img;

    Complex(const T& r, const T& i) : real{r}, img{i}
    {}
};

template <typename T1, typename T2>
Complex(T1, T2) -> Complex<std::common_type_t<T1, T2>>;

TEST_CASE("deduction witn common_type")
{
    Complex c{1, 3.14};
}

TEST_CASE("deduction guides for std objects")
{
    SECTION("pair")
    {
        pair p1{1, 5.22}; // pair<int, double>

        int tab[10];
        pair p2{"text", tab}; // pair<const char*, int*>
    }

    SECTION("tuple")
    {
        tuple t1{1, 3.14, "text"s, "abc"}; // tuple<int, double, string, const char*>
    }

    SECTION("vector")
    {
        vector v{1, 2, 3, 4};

        vector v2 = v; // vector<int>

        //vector v3(begin(v), end(v));
    }

    SECTION("array")
    {
        array arr{1, 2, 3, 4, 5, 6};
    }

    SECTION("function")
    {
        function f = [](int x) { return x * x; };
        f(2);
    }

    SECTION("smart_ptrs")
    {
        // unique_ptr u{new int(42)}; // ERROR - no deduction

        auto sp = make_shared<int>(42);

        weak_ptr wp = sp;
    }
}

template <typename T1, typename T2>
struct Aggregate
{
    T1 a;
    T2 b;
};

template <typename T1, typename T2>
Aggregate(T1, T2) -> Aggregate<T1, T2>;

TEST_CASE("Deduction for aggregates")
{
    Aggregate agg1{1, 3.14};
}
