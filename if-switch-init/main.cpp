#include <iostream>
#include <string>
#include <queue>
#include <mutex>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

TEST_CASE("if with initializer")
{
    vector<int> data = {34, 253, 53412, 245, 23524, 34563, 11, 13, 23};

    SECTION("Before C++17")
    {
        auto pos = find(begin(data), end(data), 13);

        if (pos != end(data))
            cout << *pos << " is found" << endl;
    }

    SECTION("Since C++17")
    {
        if (auto pos = find(begin(data), end(data), 13); pos != end(data))
        {
            cout << *pos << " is found" << endl;
        }
    }
}

enum class Status
{
    on,
    off,
    bad
};

struct Gadget
{
    Status status;
};

Gadget create_gadget()
{
    return Gadget{Status::on};
}

TEST_CASE("switch with initializer")
{
    switch (auto g = create_gadget(); g.status)
    {
        case Status::on:
            cout << "Gadget is on" << endl;
            break;
        case Status::off:
            cout << "Gadget is off" << endl;
            break;
        case Status::bad:
            cout << "Gadget is broken" << endl;
            break;
    }
}

TEST_CASE("use case with mutex")
{
    queue<int> q;
    mutex mtx_q;

    if (lock_guard lk{mtx_q}; !q.empty())
    {
        int value = q.front();
        q.pop();
    }
}

