#include <iostream>
#include <string>
#include <functional>

#include "catch.hpp"

using namespace std;

void foo_safe() noexcept
{
    cout << "foo_safe" << endl;
}

void foo_unsafe()
{
    cout << "foo_unsafe" << endl;
}

TEST_CASE("function ptr")
{
    void (*ptr_fun)() = &foo_safe;
    ptr_fun();

    ptr_fun = &foo_unsafe;
    ptr_fun();
}

TEST_CASE("function ptr with noexcept")
{
    void (*ptr_fun)() noexcept = &foo_safe;
    ptr_fun();

    //ptr_fun = &foo_unsafe;
    ptr_fun();

    ptr_fun = []() noexcept { cout << "lambda" << endl; };
}

template <typename F1, typename F2>
void call(F1 f1, F2 f2)
{
    f1();
    f2();
}

template <typename F>
void call(F f1, F f2)
{
    f1();
    f2();
}


TEST_CASE("Before C++17")
{
    call(foo_safe, foo_unsafe);
}
