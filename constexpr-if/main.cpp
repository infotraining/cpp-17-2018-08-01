#include <iostream>
#include <string>
#include <type_traits>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

template <typename T>
std::string as_string(T x)
{
    if constexpr(std::is_same_v<T, std::string>)
    {
        return x;
    }
    else if constexpr(std::is_arithmetic_v<T>)
    {
        return std::to_string(x);
    }
    else
        return std::string{x};
}

TEST_CASE("constexpr if")
{
    string txt = "test"s;

    auto str1 = as_string(txt);

    REQUIRE(str1 == "test"s);

    auto str2 = as_string(665);

    REQUIRE(str2 == "665"s);

    auto cstr = "cstr test";

    REQUIRE(as_string(cstr) == "cstr test");
}

template <typename T>
void process_array(const T& tab)
{
    if constexpr(std::size(tab) <= 255)
        cout << "Processing small table" << endl;
    else
        cout << "Processing large table" << endl;
}

TEST_CASE("processing arrays")
{
    int tab[42] = {};

    process_array(tab);

    array<int, 1024> arr = {{}};

    process_array(arr);

    int large_tab[6128];

    process_array(large_tab);
}

namespace BeforeCpp17
{
    void print()
    {
        cout << endl;
    }

    template <typename Head, typename... Tail>
    void print(Head head, Tail... tail)
    {
        cout << head << " ";

        print(tail...);
    }
}

namespace Cpp17
{
    template <typename Head, typename... Tail>
    void print(Head head, Tail... tail)
    {
        cout << head << " ";

        if constexpr(sizeof...(tail) > 0)
            print(tail...);
        else
            cout << endl;
    }
}

TEST_CASE("variadic templates")
{
    Cpp17::print(3.13, 4, "text"s);
}
