#include <iostream>
#include <string>

#include "catch.hpp"

using namespace std;

TEST_CASE("Before C++17")
{
    SECTION("classic decl")
    {
        int x = 1;
        int x{1};
    }

    auto x = 1;  // int
    auto y{1}; // initializer_list<int>
    //auto u{1, 1}; // initializer_list<int>
    auto z = {1}; // initializer_list<int>
}

TEST_CASE("Since C++17")
{
    auto x = 1;  // int
    auto y{1}; // int
    //auto u{1, 1}; // ERROR
    auto z = {1}; // initializer_list<int>
}


