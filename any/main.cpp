#include <iostream>
#include <string>
#include <any>

#include "catch.hpp"

using namespace std;

TEST_CASE("any")
{
    SECTION("can hold any copyable type")
    {
        any obj;

        REQUIRE_FALSE(obj.has_value());

        obj = 42;
        obj = 3.14;
        obj = "text"s;

        cout << sizeof(obj) << endl;

        SECTION("can safely return stored value")
        {
            string text = std::any_cast<string>(obj);

            REQUIRE(text == "text"s);

            REQUIRE_THROWS_AS(any_cast<double>(obj), bad_any_cast);

            SECTION("better performance with pointer")
            {
                string* ptr_text = any_cast<string>(&obj);
            }
        }

        struct X
        {
            int value;
        };

        X x{665};

        obj = &x;

        X* ptr_x = std::any_cast<X*>(obj);
    }
}
