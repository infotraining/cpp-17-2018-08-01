#include <iostream>
#include <string>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

TEST_CASE("aggregates")
{
    SECTION("native array")
    {
        int tab1[] = { 1, 2, 3 };
        int tab2[3] = {};
    }

    SECTION("struct")
    {
        struct X
        {
            string a;
            double b;
            int c[3];
        };

        X empty{};

        X x{ "1"s, 3.14, {1, 2, 3} };

        REQUIRE(x.b == 3.14);

        X y{};

        static_assert(is_aggregate_v<X>);
    }

}

template <typename T, size_t N>
struct Array
{
    T items[N];

    constexpr size_t size() const
    {
        return N;
    }

    // ...
};

TEST_CASE("Array aggregate")
{
    Array<int, 10> arr = {{}};

    std::array<int, 10> std_arr = { {1, 2, 3, 4} };
}

/////////////////////////////////////
/// \brief The WTF struct

struct WTF
{
    int a, b;

    WTF() = delete;
};


TEST_CASE("WTF")
{
    WTF wtf{};
}
