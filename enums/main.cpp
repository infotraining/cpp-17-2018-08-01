#include <iostream>
#include <string>

#include "catch.hpp"

using namespace std;

enum class Coffee : int { espresso = 5, cappucino, latte };

enum EngineType : u_int8_t { diesel, petrol, electric };

TEST_CASE("Before C++17")
{
    Coffee c = Coffee::espresso; // OK
    //Coffee c1 = 0; // ERROR
    //Coffee c2(0);  // ERROR
    Coffee c3{1}; // ERROR

    EngineType e1{1}; // ERROR
}

TEST_CASE("Since C++17")
{
    Coffee c = Coffee::espresso; // OK
    //Coffee c1 = 0; // ERROR
    //Coffee c2(0);  // ERROR
    Coffee c3{1}; // OK

    EngineType e1{1};
}

enum class Length : size_t {};

TEST_CASE("USE CASE")
{
    Length l1; // OK
    //Length l2 = 1; // ERROR
    //Length l3(1); // ERROR
    Length l4{1};
    Length l5 = Length(665);
    Length l6 = static_cast<Length>(665);
}
